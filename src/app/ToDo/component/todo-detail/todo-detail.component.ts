import { Component, OnInit } from '@angular/core';

import { Observable, Subscription } from 'rxjs';

import ToDoState from '../../todo.state';
import ToDo from '../../todo.model';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.scss']
})
export class TodoDetailComponent implements OnInit {
  todo$: Observable<ToDoState>;
  ToDoSubscription: Subscription;
  ToDoList: ToDo[] = [];
  todoError: Error = null;
  Title = '';
  IsCompleted = false;

  constructor(
    private store: Store<{ todos: ToDoState }>
  ) {
    this.todo$ = store.pipe(select('todos'));
  }

  ngOnInit(): void {
  }

}
