import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { TodoDetailComponent } from './todo-detail.component';


const routes: Routes = [
  {
    path: '',
    component: TodoDetailComponent
  }
];

@NgModule({
  declarations: [TodoDetailComponent],
  imports: [
    RouterModule.forChild(routes),
    FormsModule
  ],
  exports: [RouterModule]
})
export class ToDoDetailModule { }
