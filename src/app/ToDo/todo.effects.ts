import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as ToDoActions from './todo.action';
import ToDo from './todo.model';
import { Todos } from './todo.interface';

@Injectable()
export class ToDoEffects {
  private ApiURL = 'https://jsonplaceholder.typicode.com/users/1/todos';

  constructor(
    private http: HttpClient,
    private action$: Actions
  ) {}

  GetToDos$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(ToDoActions.BeginGetToDoAction),
      mergeMap(action =>
        this.http.get(this.ApiURL).pipe(
          map((data: Todos[]) => {
            const payload = data.map(item => {
              return { Title: item.title, IsCompleted: item.completed };
            });
            return ToDoActions.SuccessGetToDoAction({payload});
          }),
          catchError((error: Error) => {
            return this.getError(error);
          })
        )
      )
    )
  );

  CreateToDos$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(ToDoActions.BeginCreateToDoAction),
      mergeMap(action =>
        this.http.post(this.ApiURL, JSON.stringify(action.payload), {
          headers: {
            'Content-type': 'application/json; charset=UTF-8'
          }
        })
          .pipe(
            map((data: ToDo) => {
              return ToDoActions.SuccessCreateToDoAction({ payload: data });
            }),
            catchError((error: Error) => {
              return this.getError(error);
            })
          )
      )
    )
  );

  getError(error) {
    return of(ToDoActions.ErrorToDoAction(error));
  }

}
