import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromRouter from '@ngrx/router-store';

import { RouterStateUrl } from './router/router.state';
import ToDoState from './todo.state';
import { ToDoReducer } from './todo.reducer';

export interface State {
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<AppState> = {
  todos: ToDoReducer,
  router: routerReducer
};

export const selectToDoState = createFeatureSelector<
  AppState,
  RouterReducerState<RouterStateUrl>
>('todos');

export const selectRouterState = createFeatureSelector<
  AppState,
  RouterReducerState<RouterStateUrl>
>('router');

export interface AppState {
  todos: ToDoState;
  router: RouterReducerState<RouterStateUrl>;
}
