import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'todo',
    loadChildren: () => import('./ToDo/component/todo.module').then(m => m.ToDoModule)
  },
  {
    path: 'todo/:id',
    loadChildren: () => import('./ToDo/component/todo-detail/todo-detail.module').then(m => m.ToDoDetailModule)
  },
  {
    path: '',
    redirectTo: 'todo',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
