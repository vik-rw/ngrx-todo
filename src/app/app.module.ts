import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToDoReducer } from './ToDo/todo.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ToDoEffects } from './ToDo/todo.effects';
import { environment } from 'src/environments/environment';
import { CustomSerialize } from './ToDo/router/custom-serializer';
import { reducers } from './ToDo/index.state';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // StoreModule.forRoot({ todos: ToDoReducer }),
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([ToDoEffects]),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({ logOnly: environment.production })
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: CustomSerialize }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
